# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Modul Unit Test ',
    'version': '1.1',
    'category': 'Human Resources',
    'sequence': 75,
    'summary': 'Unit Test',
    'description': "",
    'depends': [
        'hr',
    ],
    'data': [
        # 'security/hr_security.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [],
    'license': 'LGPL-3',
}
